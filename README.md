Rhebo Industrial Protector Sensors
==================================

This archive contains custom sensors and channel lookup files for the Rhebo Industrial Protector system

More detals can be found in this blog article - https://blog.paessler.com/rhebo-industrial-protector-and-prtg

Prerequisites
=============

The sensors use the Rhebo API and were developed using version 2.12.1 of Rhebo Industrial Protector. 
The Rhebo API is under active development so it's possible the syntax and paths of some API calls may change over time. Please consult the Rhebo documentation for the latest information on their API. 

The sensors are written in Python 3. The following Python modules are used:

- Requests
- JSON-python-module
- HTTP.cookiejar


Installation
============

The files should be copied to the usual PRTG locations. Script files in:

C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\python

And lookups in:

C:\Program Files (x86)\PRTG Network Monitor\lookups\custom

After copying the files, remember to "refresh the lookup files" in the PRTG Administrative Tools page.

Configuration
=============

Each of the scripts must be edited to suit your environment. This is done by editing lines 31 - 33 to show the address and port number of your Rhebo server, and the user name and password for your user account. We recommend creating a dedicated, read-only account on the Rhebo system to use with the script sensors.

Several sensors use overlays containing Boolean state indicators. The states of these sensors and the corresponding messages can be edited to suit your own preferences.

Sensors
=======

Rhebo_CaptureState - Shows which Rhebo components are active

Rhebo_Diskspace - Retrieves disk useage information for the Rhebo Controller appliance

Rhebo_OperatingMode - Shows if the system is in Production or Maintenance Mode

Rhebo_NetworkScore - Shows the overall Network Quality Score, with state summary custom lookup (can be edited)

Rhebo_NetworkScoreRAW - Shows overall Network Quality Score as a number, which can be tracked over time.   
